package datastructure;

import cellular.CellState;
import java.util.Arrays;

public class CellGrid implements IGrid {
	
	private int rows;
	private int cols;
	private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.cols = columns;
		grid = new CellState[rows][columns];
		for (int i = 0; i < numRows(); i++) {
			Arrays.fill(grid[i], initialState);
		}
		
	}
    
    //copy constructor
    public CellGrid(CellGrid that) {
    	this(that.numRows(), that.numColumns(), null);
    	for (int i = 0; i < this.numRows(); i++) {
    		for (int j = 0; j < this.numColumns(); j++) {
    			this.grid[i][j] = that.grid[i][j];
    		}
    	}
    }		
    	

    @Override
    public int numRows() {
    	return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	if ((row >= 0 && row < numRows()) && (column >= 0 && column < numColumns()))
    	grid[row][column] = element;
    	else throw new IndexOutOfBoundsException("invalid row or col in CellGrid.set");
    }

    @Override
    public CellState get(int row, int column) {
    	if ((row < 0 || row >= numRows()) || (column < 0 || column >= numColumns()))
    		throw new ArrayIndexOutOfBoundsException("Illegal arguments in CellGrid.get(int, int)");
    	return grid[row][column];
    	
    }

    @Override
    public IGrid copy() {
        CellGrid newCellGrid = new CellGrid(rows, cols, null);
        newCellGrid = this;
        return newCellGrid;
    }
    



public static void main(String[] args) {
	CellGrid cellGrid = new CellGrid(10,10,CellState.ALIVE);
	for (int i = 0; i < cellGrid.rows; i++) {
		for (int j = 0; j < cellGrid.cols; j++)
			System.out.print(cellGrid.grid[i][j] + "   ");
		System.out.println();
	}
}
}

